//
//  KeyboardModel.swift
//  my-setup
//
//  Created by Lucas Gomes on 12/09/22.
//

import UIKit

class KeyboardModel: NSObject, NSCoding {
    //MARK: - Variables
    let name: String
    let brand: String
    let price: Double
    let keysPercent: Int
    
    //MARK: - Class init
    init(name: String, brand: String, price: Double, keysPercent: Int) {
        self.name = name
        self.brand = brand
        self.price = price
        self.keysPercent = keysPercent
    }
    
    //MARK: - NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(brand, forKey: "brand")
        coder.encode(price, forKey: "price")
        coder.encode(keysPercent, forKey: "keysPercent")
    }
    
    required init?(coder: NSCoder) {
        self.name = coder.decodeObject(forKey: "name") as! String
        self.brand = coder.decodeObject(forKey: "brand") as! String
        self.price = coder.decodeDouble(forKey: "price")
        self.keysPercent = coder.decodeInteger(forKey: "keysPercent")
    }
}
