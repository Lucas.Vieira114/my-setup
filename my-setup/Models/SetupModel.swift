//
//  SetupModel.swift
//  my-setup
//
//  Created by Lucas Gomes on 12/09/22.
//

import UIKit

class SetupModel: NSObject, NSCoding {
    //MARK: - Variables
    let title: String
    let mouse: MouseModel
    let keyboard: KeyboardModel
    let monitor: MonitorModel
    let computer: ComputerModel
    
    //MARK: - Class init
    init(title: String, mouse: MouseModel, keyboard: KeyboardModel, monitor: MonitorModel, computer: ComputerModel) {
        self.title = title
        self.mouse = mouse
        self.keyboard = keyboard
        self.monitor = monitor
        self.computer = computer
    }
    
    //MARK: - Methods
    func getTotalSetupPrice() -> Double {
        return mouse.price + keyboard.price + monitor.price + computer.price
    }
    
    func getDetails() -> String {
        let msgTitle = "Title: \(title)"
        let msgMouse = "Mouse: (Name: \(mouse.name)), (Brand: \(mouse.brand)), (Price: \(mouse.price))"
        let msgKeyboard = "Keyboard: (Name: \(keyboard.name)), (Brand: \(keyboard.brand)), (Price: \(keyboard.price)), (Keys Percent: \(keyboard.keysPercent)%)"
        let msgMonitor = "Monitor: (Name: \(monitor.name)), (Brand: \(monitor.brand)), (Price: \(monitor.price)), (Inch: \(monitor.inch)), (Refresh Rate: \(monitor.refreshRate))"
        let msgComputer = "Computer: (Name: \(computer.name)), (Price: \(computer.price))"
        let msgTotalValue = "Total Value: \(getTotalSetupPrice())"
        return "\(msgTitle) - \(msgMouse) - \(msgKeyboard) -\(msgMonitor) - \(msgComputer) - \(msgTotalValue)"
    }
    
    //MARK: - NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(mouse, forKey: "mouse")
        coder.encode(keyboard, forKey: "keyboard")
        coder.encode(monitor, forKey: "monitor")
        coder.encode(computer, forKey: "computer")
    }
    
    required init?(coder: NSCoder) {
        self.title = coder.decodeObject(forKey: "title") as! String
        self.mouse = coder.decodeObject(forKey: "mouse") as! MouseModel
        self.keyboard = coder.decodeObject(forKey: "keyboard") as! KeyboardModel
        self.monitor = coder.decodeObject(forKey: "monitor") as! MonitorModel
        self.computer = coder.decodeObject(forKey: "computer") as! ComputerModel
    }
}
