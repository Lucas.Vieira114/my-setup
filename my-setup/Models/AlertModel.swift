//
//  AlertModel.swift
//  my-setup
//
//  Created by Lucas Gomes on 29/09/22.
//

import UIKit

class AlertModel {
    
    //MARK: - Variable
    let controller: UIViewController
    
    //MARK: - View life cycle
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    //MARK: - Methods
    func detailsWithRemoveBtn(setup: SetupModel, removeBtnAction handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: setup.title, message: setup.getDetails(), preferredStyle: .alert)
        let cancelBtn = UIAlertAction(title: "Cancel", style: .cancel)
        let removeBtn = UIAlertAction(title: "Remove", style: .destructive, handler: handler)
        alert.addAction(cancelBtn)
        alert.addAction(removeBtn)
        controller.present(alert, animated: true)
    }
    
    func simple(title: String = "Atention", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(okBtn)
        controller.present(alert, animated: true)
    }
}
