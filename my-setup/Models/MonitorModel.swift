//
//  MonitorModel.swift
//  my-setup
//
//  Created by Lucas Gomes on 12/09/22.
//

import UIKit

class MonitorModel: NSObject, NSCoding {
    //MARK: - Variables
    let name: String
    let brand: String
    let price: Double
    let inch: Double
    let refreshRate: Int
    
    //MARK: - Class init
    init(name: String, brand: String, price: Double, inch: Double, refreshRate: Int) {
        self.name = name
        self.brand = brand
        self.price = price
        self.inch = inch
        self.refreshRate = refreshRate
    }
    
    //MARK: - NSCoding
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(brand, forKey: "brand")
        coder.encode(price, forKey: "price")
        coder.encode(inch, forKey: "inch")
        coder.encode(refreshRate, forKey: "refreshRate")
    }
    
    required init?(coder: NSCoder) {
        self.name = coder.decodeObject(forKey: "name") as! String
        self.brand = coder.decodeObject(forKey: "brand") as! String
        self.price = coder.decodeDouble(forKey: "price")
        self.inch = coder.decodeDouble(forKey: "inch")
        self.refreshRate = coder.decodeInteger(forKey: "refreshRate")
    }
}
