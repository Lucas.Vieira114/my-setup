//
//  SetupDao.swift
//  my-setup
//
//  Created by Lucas Gomes on 29/09/22.
//

import Foundation

class SetupDao {
    //MARK: - File Manager
    func updateAll(_ setups: [SetupModel]) {
        guard let path = getSetupFileManagerPath() else { return }
        
        do {
            let datas = try NSKeyedArchiver.archivedData(withRootObject: setups, requiringSecureCoding: false)
            try datas.write(to: path)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getAll() -> [SetupModel]? {
        guard let path = getSetupFileManagerPath() else { return nil }
        
        do {
            let datas = try Data(contentsOf: path)
            guard let savedSetups = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(datas) as? [SetupModel] else { return nil }
            return savedSetups
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    private func getSetupFileManagerPath() -> URL? {
        guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return directory.appendingPathComponent("Setups")
    }
}
