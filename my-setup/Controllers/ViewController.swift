//
//  ViewController.swift
//  my-setup
//
//  Created by Lucas Gomes on 12/09/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AddSetupDelegate {
    //MARK: - Variables
    var setups: [SetupModel] = [
//        SetupModel(
//            title: "Setup 1",
//            mouse: MouseModel(name: "Mouse Name", brand: "Mouse Brand", price: 10.0),
//            keyboard: KeyboardModel(name: "Keyboard Name", brand: "Keyboard Brand", price: 20.0, keysPercent: 80),
//            monitor: MonitorModel(name: "Monitor Name", brand: "Monitor Brand", price: 30.0, inch: 48.5, refreshRate: 240),
//            computer: ComputerModel(name: "Computer Name", price: 40.0)
//        )
    ]
    
    //MARK: - IBOutlet
    @IBOutlet var search: UITextField?
    @IBOutlet weak var setupTableView: UITableView?

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSetups()
    }

    //MARK: - IBAction
    @IBAction func add() {
        print("navigate to form")
        let formViewController = FormViewController(delegate: self)
        navigationController?.pushViewController(formViewController, animated: true)
    }

    //MARK: - Table data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let content = setups[indexPath.row]
        cell.textLabel?.text = content.title
        cell.accessoryType = .disclosureIndicator
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(showDetails))
        cell.addGestureRecognizer(longPress)
        return cell
    }
    
    @objc func showDetails(gesture: UIGestureRecognizer) {
        if gesture.state == .began {
            let cell = gesture.view as! UITableViewCell
            guard let indexPath = validateSetupTableView()?.indexPath(for: cell) else { return }
            let setup = setups[indexPath.row]
            
            AlertModel(controller: self).detailsWithRemoveBtn(setup: setup, removeBtnAction: { alert in self.removeSetupByIndex(indexPath.row) })
        }
    }
    
    //MARK: - Delegate methods
    func addSetup(_ setup: SetupModel) {
        setups.append(setup)
        validateSetupTableView()?.reloadData()
        SetupDao().updateAll(setups)
    }
    
    //MARK: - Methods
    func removeSetupByIndex(_ index: Int) {
        setups.remove(at: index)
        validateSetupTableView()?.reloadData()
        SetupDao().updateAll(setups)
    }

    func loadSetups() {
        guard let savedSetups = SetupDao().getAll() else { return }
        setups = savedSetups
    }
    
    //MARK: - Auxiliary
    private func validateSetupTableView() -> UITableView? {
        guard let tableView = setupTableView else {
            AlertModel(controller: self).simple(title: "Error", message: "not possible to run this operation, table not found")
            return nil
        }
        return tableView
    }
    
    //MARK: - Table view Delegate
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let cell = tableView.cellForRow(at: indexPath) else { return }
//        print("cell click")
//    }
}
