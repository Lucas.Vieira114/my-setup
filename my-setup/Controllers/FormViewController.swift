//
//  FormViewController.swift
//  my-setup
//
//  Created by Lucas Gomes on 13/09/22.
//

import UIKit

protocol AddSetupDelegate {
    func addSetup(_ setup: SetupModel)
}

class FormViewController: UIViewController {
    //MARK: - Variables
    var delegate: AddSetupDelegate?
    
    //MARK: - IBOutlet
    @IBOutlet var titleTextField: UITextField?
    
    @IBOutlet var mouseNameTextField: UITextField?
    @IBOutlet var mouseBrandTextField: UITextField?
    @IBOutlet var mousePriceTextField: UITextField?
    
    @IBOutlet var keyboardNameTextField: UITextField?
    @IBOutlet var keyboardBrandTextField: UITextField?
    @IBOutlet var keyboardPriceTextField: UITextField?
    @IBOutlet var keyboardKeyPercentTextField: UITextField?
    
    @IBOutlet var monitorNameTextField: UITextField?
    @IBOutlet var monitorBrandTextField: UITextField?
    @IBOutlet var monitorPriceTextField: UITextField?
    @IBOutlet var monitorRefreshRateTextField: UITextField?
    @IBOutlet var monitorInchTextField: UITextField?
    
    @IBOutlet var computerNameTextField: UITextField?
    @IBOutlet var computerPriceTextField: UITextField?
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    init(delegate: AddSetupDelegate) {
        super.init(nibName: "FormViewController", bundle: nil)
        self.delegate = delegate
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: - IBAction
    @IBAction func add() {
        guard let titleString = titleTextField?.text else {
            return
        }
            
        guard let mouseStringName = mouseNameTextField?.text,
              let mouseStringBrand = mouseBrandTextField?.text,
              let mouseStringPrice = mousePriceTextField?.text,
              let mouseDoublePrice = Double(mouseStringPrice) else {
            return
        }

        guard let keyboardStringName = keyboardNameTextField?.text,
              let keyboardStringBrand = keyboardBrandTextField?.text,
              let keyboardStringPrice = keyboardPriceTextField?.text,
              let keyboardStringKeyPercent = keyboardKeyPercentTextField?.text,
              let keyboardDoublePrice = Double(keyboardStringPrice),
              let keyboardIntKeyPercent = Int(keyboardStringKeyPercent) else {
            return
        }

        guard let monitorStringName = monitorNameTextField?.text,
              let monitorStringBrand = monitorBrandTextField?.text,
              let monitorStringPrice = monitorPriceTextField?.text,
              let monitorStringRefreshRate = monitorRefreshRateTextField?.text,
              let monitorStringInch = monitorInchTextField?.text,
              let monitorDoublePrice = Double(monitorStringPrice),
              let monitorIntRefreshRate = Int(monitorStringRefreshRate),
              let monitorDoubleInch = Double(monitorStringInch) else {
            return
        }

        guard let computerStringName = computerNameTextField?.text,
              let computerStringPrice = computerPriceTextField?.text,
              let computerDoublePrice = Double(computerStringPrice) else {
            return
        }
        
        let setup = SetupModel(
            title: titleString,
            mouse: MouseModel(name: mouseStringName, brand: mouseStringBrand, price: mouseDoublePrice),
            keyboard: KeyboardModel(name: keyboardStringName, brand: keyboardStringBrand, price: keyboardDoublePrice, keysPercent: keyboardIntKeyPercent),
            monitor: MonitorModel(name: monitorStringName, brand: monitorStringBrand, price: monitorDoublePrice, inch: monitorDoubleInch, refreshRate: monitorIntRefreshRate),
            computer: ComputerModel(name: computerStringName, price: computerDoublePrice)
        )
        
        delegate?.addSetup(setup)
        navigationController?.popViewController(animated: true)
    }
    
}
